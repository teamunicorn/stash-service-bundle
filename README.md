# Services Bundle Plugin for Atlassian Stash 

Adds an extendable API and a mix of services to stash to propagate repository events to different news sources.

For debug logging use:

    curl -u admin -v -X PUT -d "" -H "Content-Type: application/json" http://localhost:7990/stash/rest/api/latest/logs/logger/com.hascode/debug
    

---

** 2013 Pirate Ninja Unicorn / www.pirate-ninja-unicorn.com **