package com.hascode.stash.servicebundle.api;

import java.util.Calendar;
import java.util.Collection;

import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.repository.Repository;

public interface ChannelEvent {
	Calendar getCreated();

	Repository getRepository();

	Collection<RefChange> getChanges();

}
