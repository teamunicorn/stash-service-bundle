package com.hascode.stash.servicebundle.api;

import java.util.List;

public interface ChannelEventContentEnricher {
	String enrich(String input, ChannelEvent evt);

	List<String> getPlaceholders();
}
