package com.hascode.stash.servicebundle.api;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name = "channelProperty")
public interface ChannelProperties extends Iterable<ChannelProperty>,
		Serializable {
	@XmlTransient
	String valueFor(String key);

	@XmlElement(name = "valid")
	boolean isValid();
}
