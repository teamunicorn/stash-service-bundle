package com.hascode.stash.servicebundle.api;

import java.io.Serializable;

/**
 * Property for a channel to be set. Localize property name by
 * {pluginKey}.{moduleKey}.{propertyKey}
 */
public interface ChannelProperty extends Comparable<ChannelProperty>,
		Serializable {
	String getKey();

	String getValue();

	boolean isRequired();
}
