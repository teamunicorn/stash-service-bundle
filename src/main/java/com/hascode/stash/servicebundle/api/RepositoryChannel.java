package com.hascode.stash.servicebundle.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "repositoryChannel")
@XmlAccessorType(XmlAccessType.FIELD)
public interface RepositoryChannel {

	@XmlElement(name = "key")
	String getKey();

	@XmlElement(name = "enabled")
	boolean isEnabled();

	@XmlElement(name = "module")
	ServiceChannelModule getModule();

	@XmlElement(name = "properties")
	ChannelProperties getChannelProperties();
}
