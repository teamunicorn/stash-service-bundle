package com.hascode.stash.servicebundle.api;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name = "channelModule")
public interface ServiceChannelModule {
	@XmlElement(name = "title")
	String getTitle();

	@XmlElement(name = "description")
	String getDescription();

	@XmlTransient
	void triggerEvent(ChannelEvent evt, ChannelProperties channelProperties);
}
