package com.hascode.stash.servicebundle.api;

import static com.atlassian.plugin.util.validation.ValidationPattern.test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.BooleanUtils;
import org.dom4j.Attribute;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.util.validation.ValidationPattern;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.web.ContextProvider;
import com.atlassian.plugin.web.WebFragmentHelper;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.conditions.ConditionLoadingException;
import com.atlassian.plugin.web.descriptors.ConditionElementParser;
import com.atlassian.plugin.web.descriptors.ContextAware;
import com.atlassian.util.concurrent.NotNull;
import com.hascode.stash.servicebundle.impl.BasicChannelProperties;
import com.hascode.stash.servicebundle.impl.BasicChannelProperty;
import com.hascode.stash.servicebundle.impl.BasicRepositoryChannel;

public class ServiceChannelModuleDescriptor extends
		AbstractModuleDescriptor<ServiceChannelModule> implements ContextAware {

	private static final Logger log = LoggerFactory
			.getLogger(ServiceChannelModuleDescriptor.class);

	private WebInterfaceManager webInterfaceManager;
	private ConditionElementParser conditionElementParser;
	private ContextProviderElementParser contextProviderElementParser;
	private final WebFragmentHelper webFragmentHelper;
	private Element element;

	private boolean enabled = false;
	private ChannelProperties channelProperties;
	protected ContextProvider contextProvider;
	protected Condition condition;
	protected String link;
	protected Boolean linkAbsolute;
	protected String icon;

	public ServiceChannelModuleDescriptor(final ModuleFactory moduleFactory,
			final WebInterfaceManager webInterfaceManager,
			final WebFragmentHelper webFragmentHelper) {
		super(moduleFactory);
		setWebInterfaceManager(webInterfaceManager);
		this.webFragmentHelper = webFragmentHelper;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void init(@NotNull final Plugin plugin,
			@NotNull final Element element) throws PluginParseException {
		super.init(plugin, element);

		log.debug("initializing new service channel module...");

		this.element = element;
		enabled = this.isEnabledByDefault();

		// get parameters
		Element parametersElement = element.element("parameters");
		if (parametersElement != null) {

			Set<ChannelProperty> channelPropertySet = new LinkedHashSet<ChannelProperty>();

			Iterator<Element> parameterElementIterator = parametersElement
					.elementIterator("parameter");
			while (parameterElementIterator.hasNext()) {

				// get each parameter
				Element parameterElement = parameterElementIterator.next();
				if (parameterElement != null) {

					String key = parameterElement.attributeValue("key");
					boolean required = BooleanUtils.toBoolean(
							parameterElement.attributeValue("required"),
							"true", "false");
					ChannelProperty property = new BasicChannelProperty(key,
							null, required);
					channelPropertySet.add(property);
				}
			}

			this.channelProperties = new BasicChannelProperties(
					channelPropertySet);
		}
	}

	@Override
	public ServiceChannelModule getModule() {
		log.debug("creating new service channel module...");
		return moduleFactory.createModule(moduleClassName, this);
	}

	@Override
	protected void provideValidationRules(final ValidationPattern pattern) {
		super.provideValidationRules(pattern);
		pattern.rule(test("@class").withError(
				"The stashServiceChannel class attribute is required."));
	}

	@Override
	public void enabled() {
		super.enabled();
		this.enabled = true;

		// this was moved to the enabled() method because spring beans declared
		// by the plugin are not available for injection during the init() phase
		try {
			// check context provider
			contextProvider = contextProviderElementParser.makeContextProvider(
					plugin, element);

			// parse conditions
			condition = getRequiredConditionElementParser().makeConditions(
					plugin, element, ConditionElementParser.CompositeType.AND);

			// get link
			if (element.element("link") != null) {
				Element linkElement = element.element("link");
				link = linkElement.getTextTrim();
				Attribute linkAbsoluteAttribute = linkElement
						.attribute("absolute");
				if (linkAbsoluteAttribute != null) {
					linkAbsolute = BooleanUtils
							.toBooleanObject(linkAbsoluteAttribute.getValue());
				}
			}

			// get icon
			if (element.element("icon") != null) {
				Element iconElement = element.element("icon");
				icon = iconElement.getTextTrim();
			}

		} catch (final PluginParseException e) {
			// is there a better exception to throw?
			throw new RuntimeException(
					"Unable to enable service channel module", e);
		}
	}

	@Override
	public void disabled() {
		this.enabled = false;
		condition = null;
		webInterfaceManager.refresh();
		super.disabled();
	}

	public RepositoryChannel toRepositoryChannel() {
		return new BasicRepositoryChannel(this.getCompleteKey(),
				this.getModule(), this.getChannelProperties(), true);
	}

	public ChannelProperties getChannelProperties() {
		return channelProperties;
	}

	public void setChannelProperties(final ChannelProperties channelProperties) {
		this.channelProperties = channelProperties;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(final boolean enabled) {
		this.enabled = enabled;
	}

	public Condition getCondition() {
		return condition;
	}

	public String getIcon() {
		return icon;
	}

	public String getDisplayableIcon() {
		return getRenderedUrl(icon);
	}

	public String getLink() {
		return link;
	}

	public String getDisplayableLink() {
		return getRenderedUrl(link);
	}

	public Boolean getLinkAbsolute() {
		return linkAbsolute;
	}

	// /////////////////////////////////////////////////////////////////////////

	private String getRenderedUrl(final String url) {
		Map<String, Object> tmpContext = new HashMap<String, Object>();
		tmpContext.putAll(contextProvider
				.getContextMap(new HashMap<String, Object>()));
		String renderedUrl = webFragmentHelper.renderVelocityFragment(url,
				tmpContext);
		return renderedUrl;
	}

	private void setWebInterfaceManager(
			final WebInterfaceManager webInterfaceManager) {
		this.webInterfaceManager = webInterfaceManager;
		this.conditionElementParser = new ConditionElementParser(
				new ConditionElementParser.ConditionFactory() {
					@SuppressWarnings("deprecation")
					@Override
					public Condition create(final String className,
							final Plugin plugin)
							throws ConditionLoadingException {
						return webInterfaceManager.getWebFragmentHelper()
								.loadCondition(className, plugin);
					}
				});
		this.contextProviderElementParser = new ContextProviderElementParser(
				webInterfaceManager.getWebFragmentHelper());
	}

	private ConditionElementParser getRequiredConditionElementParser() {
		if (conditionElementParser == null) {
			throw new IllegalStateException(
					"ModuleDescriptorHelper not available because the WebInterfaceManager has not been injected.");
		} else {
			return conditionElementParser;
		}
	}

	@Override
	public ContextProvider getContextProvider() {
		return contextProvider;
	}
}
