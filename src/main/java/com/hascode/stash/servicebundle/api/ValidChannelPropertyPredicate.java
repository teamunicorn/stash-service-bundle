package com.hascode.stash.servicebundle.api;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.Predicate;

public class ValidChannelPropertyPredicate implements
		Predicate<ChannelProperty> {

	@Override
	public boolean apply(final ChannelProperty input) {
		if (input.isRequired() && StringUtils.isEmpty(input.getValue()))
			return false;
		return true;
	}

}
