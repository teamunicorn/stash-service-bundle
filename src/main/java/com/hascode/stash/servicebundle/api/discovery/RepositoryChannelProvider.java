package com.hascode.stash.servicebundle.api.discovery;

import java.util.Set;

import com.atlassian.stash.repository.Repository;
import com.hascode.stash.servicebundle.api.RepositoryChannel;
import com.hascode.stash.servicebundle.api.ServiceChannelModuleDescriptor;

public interface RepositoryChannelProvider {
	Set<ServiceChannelModuleDescriptor> channelsForRepository(
			Repository repository);

	void addChannelToRepository(RepositoryChannel channel, Repository repository);

	void removeChannelFromRepository(RepositoryChannel channel,
			Repository repository);

	void setChannelPropertiesToRepository(final RepositoryChannel channel,
			final Repository repository);
}
