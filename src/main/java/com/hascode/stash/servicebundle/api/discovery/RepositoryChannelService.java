package com.hascode.stash.servicebundle.api.discovery;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RepositoryService;
import com.hascode.stash.servicebundle.api.ServiceChannelModule;
import com.hascode.stash.servicebundle.api.ServiceChannelModuleDescriptor;

@Path("/channel")
public class RepositoryChannelService {
	private static final Logger log = LoggerFactory
			.getLogger(RepositoryChannelService.class);

	private final RepositoryChannelProvider channelRepoProvider;
	private final RepositoryService repositoryService;
	private final ServiceChannelModuleRepository serviceChannelModuleRepository;

	public RepositoryChannelService(
			final RepositoryChannelProvider channelRepoProvider,
			final RepositoryService repositoryService,
			final ServiceChannelModuleRepository serviceChannelModuleRepository) {
		this.channelRepoProvider = channelRepoProvider;
		this.repositoryService = repositoryService;
		this.serviceChannelModuleRepository = serviceChannelModuleRepository;
	}

	@GET
	@Path("/repository/{projectKey}/{repoSlug}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getChannelsForRepository(
			@PathParam("projectKey") final String projectKey,
			@PathParam("repoSlug") final String repoSlug) {
		Repository repo = repositoryService.findBySlug(projectKey, repoSlug);
		if (repo == null) {
			log.warn("no repository for {}/{}", projectKey, repoSlug);
			return Response
					.status(Status.NOT_ACCEPTABLE)
					.entity("no repository found for " + projectKey + "/"
							+ repoSlug).build();
		}

		log.debug("found repository for {}/{}: {}", new Object[] { projectKey,
				repoSlug, repo.getName() });
		Set<ServiceChannelModuleDescriptor> descriptors = channelRepoProvider
				.channelsForRepository(repo);
		Set<ServiceChannelModule> channels = new HashSet<ServiceChannelModule>();
		for (ServiceChannelModuleDescriptor descriptor : descriptors) {
			channels.add(descriptor.getModule());
		}

		if (channels == null || channels.isEmpty()) {
			log.debug("no channels for given repository found: {}/{}",
					projectKey, repoSlug);
			return Response.ok().build();
		}
		return Response.ok(channels).build();
	}

	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAvailableChannels() {
		return Response.ok(serviceChannelModuleRepository.allModules()).build();
	}
}
