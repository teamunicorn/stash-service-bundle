package com.hascode.stash.servicebundle.api.discovery;

import java.util.Set;

import com.google.common.base.Optional;
import com.hascode.stash.servicebundle.api.ServiceChannelModule;
import com.hascode.stash.servicebundle.api.ServiceChannelModuleDescriptor;

public interface ServiceChannelModuleRepository {

	Set<ServiceChannelModule> allModules();

	/**
	 * Get all enabled modules alphabetically ordered
	 * 
	 * @return A set of all enabled modules alphabetically ordered
	 */
	Set<ServiceChannelModuleDescriptor> allModuleDescriptors();

	/**
	 * Get a specific enabled ModuleDescriptor by its key
	 * 
	 * @param completeKey
	 *            Complete key of plugin module
	 * @return Option of a ServiceChannelModuleDescriptor identified by the
	 *         complete key.
	 */
	Optional<ServiceChannelModuleDescriptor> moduleDescriptorByCompleteKey(
			String completeKey);

	/**
	 * Checks whether module is enabled and my be executed
	 * 
	 * @param completeKey
	 *            Complete key of plugin module
	 * @return Whether module is enabled
	 */
	boolean isModuleEnabled(final String completeKey);
}
