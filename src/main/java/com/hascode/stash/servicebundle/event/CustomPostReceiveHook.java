package com.hascode.stash.servicebundle.event;

import java.util.Calendar;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.event.api.EventListener;
import com.atlassian.stash.event.RepositoryPushEvent;
import com.atlassian.stash.repository.Repository;
import com.google.common.collect.Lists;
import com.hascode.stash.servicebundle.api.ChannelEvent;
import com.hascode.stash.servicebundle.api.ServiceChannelModuleDescriptor;
import com.hascode.stash.servicebundle.api.discovery.RepositoryChannelProvider;
import com.hascode.stash.servicebundle.impl.BasicChannelEvent;

public class CustomPostReceiveHook {
	private static final Logger log = LoggerFactory
			.getLogger(CustomPostReceiveHook.class);
	private final RepositoryChannelProvider repositoryChannelProvider;

	public CustomPostReceiveHook(
			final RepositoryChannelProvider repositoryChannelProvider) {
		this.repositoryChannelProvider = repositoryChannelProvider;
	}

	@EventListener
	public synchronized void onPushEvent(final RepositoryPushEvent event) {
		Repository repository = event.getRepository();
		log.info(
				"services bundle post receive hook triggered for repository: {}",
				repository.getName());
		Set<ServiceChannelModuleDescriptor> channels = repositoryChannelProvider
				.channelsForRepository(repository);
		if (channels.isEmpty()) {
			log.info(
					"no channels defined for given repository {}, no event fired",
					repository.getName());
			return;
		}

		ChannelEvent channelEvent = new BasicChannelEvent(
				Calendar.getInstance(), repository, Lists.newArrayList(event
						.getRefChanges()));
		for (ServiceChannelModuleDescriptor c : channels) {
			log.debug("event delegate to channel module: "
					+ c.getModule().getTitle());
			c.getModule().triggerEvent(channelEvent, c.getChannelProperties());
		}
	}
}
