package com.hascode.stash.servicebundle.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.repository.Repository;
import com.hascode.stash.servicebundle.api.ChannelEvent;

public class BasicChannelEvent implements ChannelEvent {
	private final Calendar created;
	private final Repository repository;
	private final List<RefChange> changes;

	public BasicChannelEvent(final Calendar created,
			final Repository repository, final List<RefChange> changes) {
		this.created = created;
		this.repository = repository;
		this.changes = changes;
	}

	@Override
	public Calendar getCreated() {
		return created;
	}

	@Override
	public Repository getRepository() {
		return repository;
	}

	@Override
	public List<RefChange> getChanges() {
		return new ArrayList<RefChange>(changes);
	}

	@Override
	public String toString() {
		return "BasicChannelEvent [created=" + created + ", repository="
				+ repository + ", changes=" + changes + "]";
	}

}
