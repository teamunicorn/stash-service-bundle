package com.hascode.stash.servicebundle.impl;

import java.util.List;
import java.util.Map;

import com.atlassian.stash.view.VelocityHelper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hascode.stash.servicebundle.api.ChannelEvent;
import com.hascode.stash.servicebundle.api.ChannelEventContentEnricher;

/**
 * the description for each placeholder is to be referenced using an i18n prefix
 * of <em>com.hascode.stash.stash-service-bundle.contentenricher.</em>
 */
public class BasicChannelEventContentEnricher implements
		ChannelEventContentEnricher {
	private final List<String> placeHolders = Lists.newArrayList("repository",
			"changes", "created");
	private final VelocityHelper velocityHelper;

	public BasicChannelEventContentEnricher(final VelocityHelper velocityHelper) {
		this.velocityHelper = velocityHelper;
	}

	@Override
	public String enrich(final String input, final ChannelEvent evt) {
		Map<String, Object> renderContext = Maps.newHashMap();
		renderContext.put("repository", evt.getRepository());
		renderContext.put("changes", evt.getChanges());
		renderContext.put("created", evt.getCreated());
		return velocityHelper.renderFragment(input, renderContext);
	}

	@Override
	public List<String> getPlaceholders() {
		return placeHolders;
	}

}
