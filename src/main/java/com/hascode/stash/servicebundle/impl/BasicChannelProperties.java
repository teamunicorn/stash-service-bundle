package com.hascode.stash.servicebundle.impl;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.hascode.stash.servicebundle.api.ChannelProperties;
import com.hascode.stash.servicebundle.api.ChannelProperty;
import com.hascode.stash.servicebundle.api.ValidChannelPropertyPredicate;

public class BasicChannelProperties implements ChannelProperties {

	private static final long serialVersionUID = 1L;
	private Predicate<ChannelProperty> validChannelPropertyPredicate = new ValidChannelPropertyPredicate();
	private final Map<String, ChannelProperty> props = new LinkedHashMap<String, ChannelProperty>();

	public BasicChannelProperties() {
	}

	public BasicChannelProperties(final Set<ChannelProperty> properties) {
		for (ChannelProperty property : properties) {
			this.props.put(property.getKey(), property);
		}
	}

	@Override
	public Iterator<ChannelProperty> iterator() {
		return props.values().iterator();
	}

	public void addProperty(final ChannelProperty channelProperty) {
		props.put(channelProperty.getKey(), channelProperty);
	}

	public ChannelProperty getProperty(final String key) {
		return props.get(key);
	}

	@Override
	public String toString() {
		return "BasicChannelProperties [props=" + props + "]";
	}

	@Override
	public String valueFor(final String key) {
		return (getProperty(key) == null) ? null : getProperty(key).getValue();
	}

	@Override
	public boolean isValid() {
		return Iterables.all(props.values(), validChannelPropertyPredicate);
	}

	public final void setValidChannelPropertyPredicate(
			final Predicate<ChannelProperty> validChannelPropertyPredicate) {
		this.validChannelPropertyPredicate = validChannelPropertyPredicate;
	}
}
