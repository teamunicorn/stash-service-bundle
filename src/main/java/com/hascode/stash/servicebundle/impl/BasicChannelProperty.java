package com.hascode.stash.servicebundle.impl;

import com.hascode.stash.servicebundle.api.ChannelProperty;

public class BasicChannelProperty implements ChannelProperty {
	private static final long serialVersionUID = 1L;
	private final String key;
	private final String value;
	private final boolean required;

	public BasicChannelProperty(final String key, final String value,
			final boolean required) {
		this.key = key;
		this.value = value;
		this.required = required;
	}

	@Override
	public int compareTo(final ChannelProperty o) {
		return key.compareTo(o.getKey());
	}

	@Override
	public String getKey() {
		return key;
	}

	@Override
	public String getValue() {
		return value;
	}

	@Override
	public boolean isRequired() {
		return required;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BasicChannelProperty other = (BasicChannelProperty) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BasicChannelProperty [key=" + key + ", value=" + value
				+ ", required=" + required + "]";
	}

}
