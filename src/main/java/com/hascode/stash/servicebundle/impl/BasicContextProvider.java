package com.hascode.stash.servicebundle.impl;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;

public class BasicContextProvider implements ContextProvider {

	private final Map<String, Object> contextData = new HashMap<String, Object>();

	@Override
	public void init(final Map<String, String> params)
			throws PluginParseException {
		contextData.putAll(params);
	}

	@Override
	public Map<String, Object> getContextMap(final Map<String, Object> context) {
		contextData.putAll(context);
		return contextData;
	}

}
