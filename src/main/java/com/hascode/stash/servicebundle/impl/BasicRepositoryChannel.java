package com.hascode.stash.servicebundle.impl;

import com.hascode.stash.servicebundle.api.ChannelProperties;
import com.hascode.stash.servicebundle.api.RepositoryChannel;
import com.hascode.stash.servicebundle.api.ServiceChannelModule;

public class BasicRepositoryChannel implements RepositoryChannel {

	private final String key;
	private boolean enabled = false;
	private final ServiceChannelModule module;
	private final ChannelProperties channelProperties;

	public BasicRepositoryChannel(final String key,
			final ServiceChannelModule module,
			final ChannelProperties channelProperties, final boolean enabled) {
		this.key = key;
		this.module = module;
		this.channelProperties = channelProperties;
		this.enabled = enabled;
	}

	@Override
	public String getKey() {
		return key;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public ServiceChannelModule getModule() {
		return module;
	}

	@Override
	public ChannelProperties getChannelProperties() {
		return channelProperties;
	}
}
