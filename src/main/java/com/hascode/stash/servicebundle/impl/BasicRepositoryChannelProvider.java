package com.hascode.stash.servicebundle.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.stash.repository.Repository;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.hascode.stash.servicebundle.api.ChannelProperties;
import com.hascode.stash.servicebundle.api.ChannelProperty;
import com.hascode.stash.servicebundle.api.RepositoryChannel;
import com.hascode.stash.servicebundle.api.ServiceChannelModuleDescriptor;
import com.hascode.stash.servicebundle.api.discovery.RepositoryChannelProvider;
import com.hascode.stash.servicebundle.api.discovery.ServiceChannelModuleRepository;

public class BasicRepositoryChannelProvider implements
		RepositoryChannelProvider {

	private static Logger log = LoggerFactory
			.getLogger(BasicRepositoryChannelProvider.class);

	/**
	 * Key to save active channels for a repository.
	 */
	private static final String ACTIVE_CHANNELS_KEY = "hascode.servicebundle.channels.";

	/**
	 * Key to save channel properties for a repository-channel-combination.
	 */
	private static final String CHANNEL_PROPERTIES_BASE_KEY = "hascode.servicebundle.channel.";

	private final PluginSettingsFactory pluginSettingsFactory;
	private final ServiceChannelModuleRepository serviceChannelModuleRepository;

	public BasicRepositoryChannelProvider(
			final PluginSettingsFactory pluginSettingsFactory,
			final ServiceChannelModuleRepository serviceChannelModuleRepository) {
		this.pluginSettingsFactory = pluginSettingsFactory;
		this.serviceChannelModuleRepository = serviceChannelModuleRepository;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<ServiceChannelModuleDescriptor> channelsForRepository(
			final Repository repository) {

		Set<ServiceChannelModuleDescriptor> descriptorSet = new HashSet<ServiceChannelModuleDescriptor>();
		PluginSettings globalSettings = pluginSettingsFactory
				.createGlobalSettings();
		List<String> channelKeyList = (List<String>) globalSettings
				.get(ACTIVE_CHANNELS_KEY + repository.getSlug());
		if (channelKeyList == null) {
			return descriptorSet;
		}

		// retrieve channel objects
		for (String channelKey : channelKeyList) {
			Optional<ServiceChannelModuleDescriptor> moduleDescriptorOption = serviceChannelModuleRepository
					.moduleDescriptorByCompleteKey(channelKey);
			if (!moduleDescriptorOption.isPresent()) {
				continue;
			}

			ServiceChannelModuleDescriptor moduleDescriptor = moduleDescriptorOption
					.get();

			// retrieve properties
			PluginSettings moduleSettings = pluginSettingsFactory
					.createSettingsForKey(CHANNEL_PROPERTIES_BASE_KEY
							+ repository.getSlug() + "."
							+ moduleDescriptor.getCompleteKey());
			BasicChannelProperties properties = (BasicChannelProperties) moduleDescriptor
					.getChannelProperties();
			for (ChannelProperty property : properties) {
				String value = (String) moduleSettings.get(property.getKey());
				properties.addProperty(new BasicChannelProperty(property
						.getKey(), value, property.isRequired()));
			}
			moduleDescriptor.setChannelProperties(properties);

			// TODO: add context information to context provider on each module
			moduleDescriptor.getContextProvider().getContextMap(
					ImmutableMap.of("repository", (Object) repository));

			descriptorSet.add(moduleDescriptor);
		}
		return descriptorSet;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void addChannelToRepository(final RepositoryChannel channel,
			final Repository repository) {
		PluginSettings globalSettings = pluginSettingsFactory
				.createGlobalSettings();
		List<String> channelKeyList = (List<String>) globalSettings
				.get(ACTIVE_CHANNELS_KEY + repository.getSlug());
		if (channelKeyList == null) {
			channelKeyList = new ArrayList<String>();
		}

		channelKeyList.add(channel.getKey());
		globalSettings.put(ACTIVE_CHANNELS_KEY + repository.getSlug(),
				channelKeyList);

		// save parameters
		setChannelPropertiesToRepository(channel, repository);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void removeChannelFromRepository(final RepositoryChannel channel,
			final Repository repository) {

		PluginSettings globalSettings = pluginSettingsFactory
				.createGlobalSettings();
		List<String> channelKeyList = (List<String>) globalSettings
				.get(ACTIVE_CHANNELS_KEY + repository.getSlug());
		if (Iterables.isEmpty(channelKeyList)) {
			return;
		}

		// remove channel
		channelKeyList.remove(channel.getKey());

		// remove all properties, too
		PluginSettings moduleSettings = pluginSettingsFactory
				.createSettingsForKey(CHANNEL_PROPERTIES_BASE_KEY
						+ repository.getSlug() + "." + channel.getKey());
		ChannelProperties properties = channel.getChannelProperties();
		for (ChannelProperty property : properties) {
			moduleSettings.remove(property.getKey());
		}

		log.info("Deleting module {} from repository {}, remaining: {}",
				new Object[] { channel, repository, channelKeyList });

		globalSettings.put(ACTIVE_CHANNELS_KEY + repository.getSlug(),
				channelKeyList);
	}

	@Override
	public void setChannelPropertiesToRepository(
			final RepositoryChannel channel, final Repository repository) {
		PluginSettings moduleSettings = pluginSettingsFactory
				.createSettingsForKey(CHANNEL_PROPERTIES_BASE_KEY
						+ repository.getSlug() + "." + channel.getKey());
		ChannelProperties properties = channel.getChannelProperties();
		for (ChannelProperty property : properties) {
			if (!StringUtils.isEmpty(property.getValue())) {
				moduleSettings.put(property.getKey(), property.getValue());
			}
		}
	}
}
