package com.hascode.stash.servicebundle.impl;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.google.common.base.Optional;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.hascode.stash.servicebundle.api.ServiceChannelModule;
import com.hascode.stash.servicebundle.api.ServiceChannelModuleDescriptor;
import com.hascode.stash.servicebundle.api.discovery.ServiceChannelModuleRepository;
import com.hascode.stash.servicebundle.util.AlphabeticalModuleDescriptorComparator;

public class BasicServiceChannelModuleRepository implements
		ServiceChannelModuleRepository {
	private static final Logger log = LoggerFactory
			.getLogger(BasicServiceChannelModuleRepository.class);

	private final PluginAccessor pluginAccessor;
	private final AlphabeticalModuleDescriptorComparator comparator = new AlphabeticalModuleDescriptorComparator();

	public BasicServiceChannelModuleRepository(
			final PluginAccessor pluginAccessor) {
		this.pluginAccessor = pluginAccessor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hascode.stash.servicebundle.api.discovery.ServiceChannelModuleRepository
	 * #allModules()
	 */
	@Override
	public Set<ServiceChannelModule> allModules() {
		final Set<ServiceChannelModule> modules = Sets.newHashSet();
		final List<ServiceChannelModuleDescriptor> serviceChannelModuleDescriptors = pluginAccessor
				.getEnabledModuleDescriptorsByClass(ServiceChannelModuleDescriptor.class);
		if (Iterables.isEmpty(serviceChannelModuleDescriptors)) {
			log.warn("no filter module found");
			return modules;
		}

		log.debug("parsing {} service channel module descriptors",
				serviceChannelModuleDescriptors.size());
		for (final ServiceChannelModuleDescriptor serviceChannelModuleDescriptor : serviceChannelModuleDescriptors) {
			final ServiceChannelModule module = serviceChannelModuleDescriptor
					.getModule();
			if (null == module) {
				log.warn("a service channel module could not be initialized");
				continue;
			}

			log.debug("adding service channel module to the stack: {} ({})",
					module.getDescription(), module.getClass());
			modules.add(module);
		}
		return modules;
	}

	@Override
	public Set<ServiceChannelModuleDescriptor> allModuleDescriptors() {
		final Set<ServiceChannelModuleDescriptor> descriptors = new TreeSet<ServiceChannelModuleDescriptor>(
				comparator);
		List<ServiceChannelModuleDescriptor> enabledModuleDescriptors = pluginAccessor
				.getEnabledModuleDescriptorsByClass(ServiceChannelModuleDescriptor.class);
		if (!Iterables.isEmpty(enabledModuleDescriptors)) {
			descriptors.addAll(enabledModuleDescriptors);
		}
		return descriptors;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hascode.stash.servicebundle.api.discovery.ServiceChannelModuleRepository
	 * #moduleDescriptorByCompleteKey(java.lang.String)
	 */
	@Override
	public Optional<ServiceChannelModuleDescriptor> moduleDescriptorByCompleteKey(
			final String completeKey) {
		ModuleDescriptor<?> moduleDescriptor = pluginAccessor
				.getPluginModule(completeKey);
		if (moduleDescriptor instanceof ServiceChannelModuleDescriptor) {
			return Optional
					.of((ServiceChannelModuleDescriptor) moduleDescriptor);
		}
		return Optional.absent();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hascode.stash.servicebundle.api.discovery.ServiceChannelModuleRepository
	 * #isModuleEnabled(java.lang.String)
	 */
	@Override
	public boolean isModuleEnabled(final String completeKey) {
		return pluginAccessor.isPluginModuleEnabled(completeKey);
	}
}
