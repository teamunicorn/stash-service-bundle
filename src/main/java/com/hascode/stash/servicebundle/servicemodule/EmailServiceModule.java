package com.hascode.stash.servicebundle.servicemodule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.stash.mail.MailMessage;
import com.atlassian.stash.mail.MailService;
import com.hascode.stash.servicebundle.api.ChannelEvent;
import com.hascode.stash.servicebundle.api.ChannelEventContentEnricher;
import com.hascode.stash.servicebundle.api.ChannelProperties;
import com.hascode.stash.servicebundle.api.ServiceChannelModule;

public class EmailServiceModule implements ServiceChannelModule {
	private static final String KEY_TEMPLATE = "template";
	private static final String KEY_SUBJECT = "subject";
	private static final String KEY_RECIPIENT = "recipient";
	private static final String KEY_SENDER = "sender";

	private static final Logger log = LoggerFactory
			.getLogger(EmailServiceModule.class);

	private final MailService mailService;
	private final ChannelEventContentEnricher enricher;

	public EmailServiceModule(final MailService mailService,
			final ChannelEventContentEnricher enricher) {
		this.mailService = mailService;
		this.enricher = enricher;
	}

	@Override
	public String getTitle() {
		return "E-Mail Service";
	}

	@Override
	public String getDescription() {
		return "Pushes news to an E-Mail post box";
	}

	@Override
	public void triggerEvent(final ChannelEvent evt,
			final ChannelProperties channelProperties) {
		log.info(
				"channel event caught in e-mail module: {} with properties: {}",
				evt.toString(), channelProperties.toString());
		if (!mailService.isHostConfigured()) {
			log.warn("no host is configured for the e-mail service. please add an e-mail server first.");
			return;
		}

		if (!channelProperties.isValid()) {
			log.warn("not all required channel properties are defined - skipping further processing.");
			return;
		}
		sendMail(evt, channelProperties);
	}

	private void sendMail(final ChannelEvent evt, final ChannelProperties props) {
		final String mailBody = enricher.enrich(props.valueFor(KEY_TEMPLATE),
				evt);
		final String subject = enricher
				.enrich(props.valueFor(KEY_SUBJECT), evt);
		MailMessage message = new MailMessage.Builder()
				.from(props.valueFor(KEY_SENDER))
				.to(props.valueFor(KEY_RECIPIENT)).subject(subject)
				.text(mailBody).build();
		log.debug("sending mail to {} with message: {}",
				props.valueFor(KEY_RECIPIENT), mailBody);
		mailService.submit(message);
	}

}
