package com.hascode.stash.servicebundle.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RepositoryService;
import com.atlassian.stash.user.Permission;
import com.atlassian.stash.user.PermissionValidationService;
import com.google.common.base.Optional;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.hascode.stash.servicebundle.api.ChannelEventContentEnricher;
import com.hascode.stash.servicebundle.api.ChannelProperties;
import com.hascode.stash.servicebundle.api.ChannelProperty;
import com.hascode.stash.servicebundle.api.RepositoryChannel;
import com.hascode.stash.servicebundle.api.ServiceChannelModuleDescriptor;
import com.hascode.stash.servicebundle.api.discovery.RepositoryChannelProvider;
import com.hascode.stash.servicebundle.api.discovery.ServiceChannelModuleRepository;
import com.hascode.stash.servicebundle.impl.BasicChannelProperties;
import com.hascode.stash.servicebundle.impl.BasicChannelProperty;
import com.hascode.stash.servicebundle.impl.BasicRepositoryChannel;

public class RepositoryPublisherServlet extends HttpServlet {

	private static final long serialVersionUID = -8213968873773405092L;

	private static Logger log = LoggerFactory
			.getLogger(RepositoryPublisherServlet.class);

	private static final String PARAMETER_ACTION = "action";
	private static final String PARAMETER_ACTION_ADD = "add";
	private static final String PARAMETER_ACTION_SAVE = "save";
	private static final String PARAMETER_ACTION_REMOVE = "remove";
	private static final String PARAMETER_SELECTED_MODULE = "module";

	private static final String CONTEXT_REPOSITORY = "repository";
	private static final String CONTEXT_ACTIVE_CHANNELS = "activeChannels";
	private static final String CONTEXT_SELECTED_CHANNEL = "selectedChannel";
	private static final String CONTEXT_AVAILABLE_CHANNELS = "availableChannels";
	private static final String CONTEXT_BASE_URL = "baseUrl";
	private static final String CONTEXT_PLACEHOLDERS = "placeHolders";

	private static final String CHANNEL_PROPERTY_PREFIX = "param_";

	private static final String RESPONSE_CONTENT_TYPE = "text/html;charset=UTF-8";
	private static final String RESPONSE_TEMPLATE_NAME = "stash.page.servicebundle.templates.repoPublishers";
	private static final String RESPONSE_MODULE_KEY = "com.hascode.stash.stash-service-bundle:soy-templates";

	private final SoyTemplateRenderer soyTemplateRenderer;
	private final RepositoryService repositoryService;
	private final PermissionValidationService permissionValidationService;
	private final NavBuilder navBuilder;

	private final ServiceChannelModuleRepository serviceChannelModuleRepository;
	private final RepositoryChannelProvider repositoryChannelProvider;
	private final ChannelEventContentEnricher contentEnricher;

	public RepositoryPublisherServlet(
			final SoyTemplateRenderer soyTemplateRenderer,
			final RepositoryService repositoryService,
			final PermissionValidationService permissionValidationService,
			final ServiceChannelModuleRepository serviceChannelModuleRepository,
			final RepositoryChannelProvider repositoryChannelProvider,
			final ChannelEventContentEnricher contentEnricher,
			final NavBuilder navBuilder) {
		this.soyTemplateRenderer = soyTemplateRenderer;
		this.repositoryService = repositoryService;
		this.permissionValidationService = permissionValidationService;
		this.serviceChannelModuleRepository = serviceChannelModuleRepository;
		this.repositoryChannelProvider = repositoryChannelProvider;
		this.contentEnricher = contentEnricher;
		this.navBuilder = navBuilder;
	}

	@Override
	protected void doGet(final HttpServletRequest req,
			final HttpServletResponse resp) throws ServletException,
			IOException {

		// get current repository
		Optional<Repository> repositoryOption = getCurrentRepository(req);
		if (!repositoryOption.isPresent()) {
			resp.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		Map<String, Object> contextMap = createBasicContext(req,
				repositoryOption);
		render(resp, contextMap);
	}

	/**
	 * Add/remove module
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void doPost(final HttpServletRequest req,
			final HttpServletResponse resp) throws ServletException,
			IOException {

		// get current repository
		Optional<Repository> repositoryOption = getCurrentRepository(req);
		if (!repositoryOption.isPresent()) {
			resp.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}

		Map<String, Object> contextMap = createBasicContext(req,
				repositoryOption);
		Repository repository = (Repository) contextMap.get(CONTEXT_REPOSITORY);
		List<ServiceChannelModuleDescriptor> activeChannels = (List<ServiceChannelModuleDescriptor>) contextMap
				.get(CONTEXT_ACTIVE_CHANNELS);
		ServiceChannelModuleDescriptor selectedChannel = (ServiceChannelModuleDescriptor) contextMap
				.get(CONTEXT_SELECTED_CHANNEL);

		// TODO: handle selectedChannel==NULL in each action
		String action = req.getParameter(PARAMETER_ACTION);
		if (PARAMETER_ACTION_ADD.equals(action)) {
			// add and select
			Optional<ServiceChannelModuleDescriptor> moduleDescriptorOption = addChannelToRepository(
					req, selectedChannel, repository);
			if (moduleDescriptorOption.isPresent()) {
				selectedChannel = moduleDescriptorOption.get();
			}
		}
		if (PARAMETER_ACTION_SAVE.equals(action)) {
			// save
			getChannelPropertiesFromRequest(req, selectedChannel);
			repositoryChannelProvider.setChannelPropertiesToRepository(
					selectedChannel.toRepositoryChannel(), repository);
		}
		if (PARAMETER_ACTION_REMOVE.equals(action)) {
			// remove and select first descriptor again
			repositoryChannelProvider.removeChannelFromRepository(
					selectedChannel.toRepositoryChannel(), repository);
			selectedChannel = null;
		}

		// update active channels
		activeChannels = Lists.newArrayList(repositoryChannelProvider
				.channelsForRepository(repository));

		// get available module descriptors
		List<ServiceChannelModuleDescriptor> availableChannels = Lists
				.newLinkedList(serviceChannelModuleRepository
						.allModuleDescriptors());
		availableChannels.removeAll(activeChannels);

		// render response
		contextMap = generateContextMap(repository, availableChannels,
				activeChannels, selectedChannel);
		contextMap.put(PARAMETER_ACTION, action);
		render(resp, contextMap);
	}

	private Map<String, Object> createBasicContext(
			final HttpServletRequest req,
			final Optional<Repository> repositoryOption) {
		Repository repository = repositoryOption.get();

		// check permission
		permissionValidationService.validateForRepository(repository,
				Permission.REPO_ADMIN);

		// get activated module descriptors
		List<ServiceChannelModuleDescriptor> activeChannels = Lists
				.newLinkedList(repositoryChannelProvider
						.channelsForRepository(repository));

		// get available module descriptors
		List<ServiceChannelModuleDescriptor> availableChannels = Lists
				.newLinkedList(serviceChannelModuleRepository
						.allModuleDescriptors());
		availableChannels.removeAll(activeChannels);

		// get currently selected module descriptor
		ServiceChannelModuleDescriptor selectedChannel = getSelectedChannel(
				req, activeChannels);

		// render response
		Map<String, Object> contextMap = generateContextMap(repository,
				availableChannels, activeChannels, selectedChannel);
		return contextMap;
	}

	private Map<String, Object> generateContextMap(final Repository repository,
			final List<ServiceChannelModuleDescriptor> availableChannels,
			final List<ServiceChannelModuleDescriptor> activeChannels,
			final ServiceChannelModuleDescriptor selectedChannel) {
		Map<String, Object> contextMap = new HashMap<String, Object>();
		contextMap.put(CONTEXT_REPOSITORY, repository);
		contextMap.put(CONTEXT_AVAILABLE_CHANNELS, availableChannels);
		contextMap.put(CONTEXT_ACTIVE_CHANNELS, activeChannels);
		contextMap.put(CONTEXT_BASE_URL, navBuilder.buildAbsolute());
		if (selectedChannel != null) {
			contextMap.put(CONTEXT_SELECTED_CHANNEL, selectedChannel);
		}
		contextMap.put(CONTEXT_PLACEHOLDERS, contentEnricher.getPlaceholders());
		return contextMap;
	}

	private Optional<ServiceChannelModuleDescriptor> addChannelToRepository(
			final HttpServletRequest req,
			final ServiceChannelModuleDescriptor selectedChannel,
			final Repository repository) {

		ChannelProperties channelProperties = getChannelPropertiesFromRequest(
				req, selectedChannel);
		RepositoryChannel channel = new BasicRepositoryChannel(
				selectedChannel.getCompleteKey(), selectedChannel.getModule(),
				channelProperties, true);

		// add descriptor
		log.info("found {} for {}", selectedChannel,
				selectedChannel.getCompleteKey());
		repositoryChannelProvider.addChannelToRepository(channel, repository);

		return Optional.of(selectedChannel);
	}

	private Optional<Repository> getCurrentRepository(
			final HttpServletRequest req) throws IOException {

		// get path info
		String pathInfo = req.getPathInfo();
		if (Strings.isNullOrEmpty(pathInfo) || pathInfo.equals("/")) {
			return Optional.absent();
		}
		String[] pathParts = pathInfo.substring(1).split("/");
		if (pathParts.length != 2) {
			return Optional.absent();
		}

		// get repository
		String projectKey = pathParts[0];
		String repoSlug = pathParts[1];
		Repository repository = repositoryService.findBySlug(projectKey,
				repoSlug);
		if (repository == null) {
			return Optional.absent();
		}
		return Optional.of(repository);
	}

	private ChannelProperties getChannelPropertiesFromRequest(
			final HttpServletRequest req,
			final ServiceChannelModuleDescriptor selectedChannel) {
		BasicChannelProperties channelProperties = (BasicChannelProperties) selectedChannel
				.getChannelProperties();

		// ////////////////////////
		// get parameters
		for (ChannelProperty property : channelProperties) {

			// get value for parameter
			BasicChannelProperty basicProperty = (BasicChannelProperty) property;
			String parameterValue = req.getParameter(CHANNEL_PROPERTY_PREFIX
					+ basicProperty.getKey());
			if (!StringUtils.isEmpty(parameterValue)) {
				basicProperty = new BasicChannelProperty(
						basicProperty.getKey(), parameterValue,
						basicProperty.isRequired());
			} else {
				basicProperty = new BasicChannelProperty(
						basicProperty.getKey(), "", basicProperty.isRequired());
			}
			channelProperties.addProperty(basicProperty);
		}

		return channelProperties;
	}

	private ServiceChannelModuleDescriptor getSelectedChannel(
			final HttpServletRequest req,
			final List<ServiceChannelModuleDescriptor> channels) {

		// get module descriptor if selected
		String moduleKey = req.getParameter(PARAMETER_SELECTED_MODULE);
		if (!StringUtils.isEmpty(moduleKey)) {
			Optional<ServiceChannelModuleDescriptor> moduleDescriptorOption = serviceChannelModuleRepository
					.moduleDescriptorByCompleteKey(moduleKey);
			if (moduleDescriptorOption.isPresent()) {
				log.info("Module selected {}", moduleDescriptorOption.get());
				return moduleDescriptorOption.get();
			}
		}

		// none selected? choose first module
		if (!Iterables.isEmpty(channels)) {
			log.info("No module selected, display first {}", channels.get(0));
			return channels.get(0);
		}

		return null;
	}

	private void render(final HttpServletResponse resp,
			final Map<String, Object> data) throws IOException,
			ServletException {
		// webResourceManager
		// .requireResourcesForContext("stash.page.repository.publisher");
		resp.setContentType(RESPONSE_CONTENT_TYPE);
		try {
			soyTemplateRenderer.render(resp.getWriter(), RESPONSE_MODULE_KEY,
					RESPONSE_TEMPLATE_NAME, data);
		} catch (SoyException e) {
			Throwable cause = e.getCause();
			if (cause instanceof IOException) {
				throw (IOException) cause;
			}
			throw new ServletException(e);
		}
	}
}
