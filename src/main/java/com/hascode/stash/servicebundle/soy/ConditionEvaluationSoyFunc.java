package com.hascode.stash.servicebundle.soy;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.soy.renderer.SoyServerFunction;
import com.google.common.collect.ImmutableSet;
import com.hascode.stash.servicebundle.api.ServiceChannelModuleDescriptor;

public class ConditionEvaluationSoyFunc implements
		SoyServerFunction<Boolean> {

	private static Logger log = LoggerFactory
			.getLogger(ConditionEvaluationSoyFunc.class);

	private static final String SOY_FUNCTION_NAME = "evaluateConditions";

	public ConditionEvaluationSoyFunc() {
	}

	@Override
	public String getName() {
		return SOY_FUNCTION_NAME;
	}

	@Override
	public Boolean apply(final Object... args) {

		if (args[0] == null) {
			return null;
		}
		if (!(args[0] instanceof ServiceChannelModuleDescriptor)) {
			return null;
		}

		ServiceChannelModuleDescriptor descriptor = (ServiceChannelModuleDescriptor) args[0];

		if (descriptor.getCondition() == null) {
			return true;
		}

		try {
			return descriptor.getCondition().shouldDisplay(null);
		} catch (Throwable t) {
			log.error(
					"Could not evaluate condition '"
							+ descriptor.getCondition() + "' for descriptor: "
							+ descriptor, t);
		}
		return false;
	}

	@Override
	public Set<Integer> validArgSizes() {
		return ImmutableSet.of(1);
	}

}
