package com.hascode.stash.servicebundle.soy;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.soy.renderer.SoyServerFunction;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;
import com.hascode.stash.servicebundle.api.ChannelProperty;
import com.hascode.stash.servicebundle.api.ValidChannelPropertyPredicate;

public class ValidatePropertySoyFunc implements SoyServerFunction<Boolean> {

	private static Logger log = LoggerFactory
			.getLogger(ValidatePropertySoyFunc.class);

	private static final String SOY_FUNCTION_NAME = "validateProperty";

	private final Predicate<ChannelProperty> predicate;

	public ValidatePropertySoyFunc() {
		predicate = new ValidChannelPropertyPredicate();
	}

	@Override
	public String getName() {
		return SOY_FUNCTION_NAME;
	}

	@Override
	public Boolean apply(final Object... args) {

		if (args[0] == null) {
			return null;
		}
		if (!(args[0] instanceof ChannelProperty)) {
			return null;
		}

		ChannelProperty channelProperty = (ChannelProperty) args[0];
		return predicate.apply(channelProperty);
	}

	@Override
	public Set<Integer> validArgSizes() {
		return ImmutableSet.of(1);
	}

}
