package com.hascode.stash.servicebundle.soy;

import java.util.Map;

public class WebItemData {

	// for Soy templates
	public enum Type {
		ITEM, SECTION
	}

	private final String url;
	private final String pluginKey;
	private final String moduleKey;
	private final String linkText;
	private final String tooltip;
	private final String iconUrl;
	private final int iconWidth;
	private final int iconHeight;
	private final String styleClass;
	private final String linkId;
	private final String description;
	private final Map<String, String> params;

	public WebItemData(final String url, final String pluginKey,
			final String moduleKey, final String linkText,
			final String tooltip, final String iconUrl, final int iconWidth,
			final int iconHeight, final String styleClass, final String linkId,
			final String description, final Map<String, String> params) {
		this.url = url;
		this.pluginKey = pluginKey;
		this.moduleKey = moduleKey;
		this.linkText = linkText;
		this.tooltip = tooltip;
		this.iconUrl = iconUrl;
		this.iconWidth = iconWidth;
		this.iconHeight = iconHeight;
		this.styleClass = styleClass;
		this.linkId = linkId;
		this.description = description;
		this.params = params;
	}

	public String getUrl() {
		return url;
	}

	public String getLinkText() {
		return linkText;
	}

	public String getTooltip() {
		return tooltip;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public boolean getHasTooltip() {
		return tooltip != null;
	}

	public int getIconWidth() {
		return iconWidth;
	}

	public int getIconHeight() {
		return iconHeight;
	}

	public String getStyleClass() {
		return styleClass;
	}

	public String getLinkId() {
		return linkId;
	}

	public String getDescription() {
		return description;
	}

	public String getPluginKey() {
		return pluginKey;
	}

	public String getModuleKey() {
		return moduleKey;
	}

	public String getCompleteModuleKey() {
		return String.format("%s:%s", getPluginKey(), getModuleKey());
	}

	public Map<String, String> getParams() {
		return params;
	}

	public String getType() {
		return Type.ITEM.name();
	}

}