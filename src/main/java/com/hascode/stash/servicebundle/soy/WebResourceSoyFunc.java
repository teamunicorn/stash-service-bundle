package com.hascode.stash.servicebundle.soy;

import java.util.Set;

import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.soy.renderer.SoyServerFunction;
import com.google.common.collect.ImmutableSet;

public class WebResourceSoyFunc implements SoyServerFunction<String> {

	private static final String SOY_FUNCTION_NAME = "getWebResource";

	private final WebResourceUrlProvider urlProvider;

	public WebResourceSoyFunc(final WebResourceUrlProvider urlProvider) {
		this.urlProvider = urlProvider;
	}

	@Override
	public String getName() {
		return SOY_FUNCTION_NAME;
	}

	@Override
	public String apply(final Object... args) {

		if (args[0] == null) {
			return "";
		}
		String resourceName = args[0].toString();

		return urlProvider
				.getStaticPluginResourceUrl(
						"com.hascode.stash.servicebundle:stash-service-bundle-resources",
						resourceName, UrlMode.RELATIVE);
	}

	@Override
	public Set<Integer> validArgSizes() {
		return ImmutableSet.of(1);
	}
}
