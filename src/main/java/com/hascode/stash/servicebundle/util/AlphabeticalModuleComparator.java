package com.hascode.stash.servicebundle.util;

import java.util.Comparator;

import com.hascode.stash.servicebundle.api.ServiceChannelModule;

public class AlphabeticalModuleComparator implements
		Comparator<ServiceChannelModule> {

	@Override
	public int compare(final ServiceChannelModule m1,
			final ServiceChannelModule m2) {

		if (m1 == null)
			return -1;
		if (m2 == null)
			return 1;

		return m1.getTitle().compareTo(m2.getTitle());
	}

}
