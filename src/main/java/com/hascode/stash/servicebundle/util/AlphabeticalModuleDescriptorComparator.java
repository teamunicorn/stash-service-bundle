package com.hascode.stash.servicebundle.util;

import java.util.Comparator;

import com.hascode.stash.servicebundle.api.ServiceChannelModuleDescriptor;

public class AlphabeticalModuleDescriptorComparator implements
		Comparator<ServiceChannelModuleDescriptor> {

	@Override
	public int compare(final ServiceChannelModuleDescriptor d1,
			final ServiceChannelModuleDescriptor d2) {

		if (d1 == null)
			return -1;
		if (d2 == null)
			return 1;
		if (d1.getModule() == null)
			return -1;
		if (d2.getModule() == null)
			return 1;

		return d1.getModule().getTitle().compareTo(d2.getModule().getTitle());
	}

}
